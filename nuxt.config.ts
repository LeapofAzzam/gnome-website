import { defineNuxtConfig } from 'nuxt'
import svgLoader from "vite-svg-loader"

export default defineNuxtConfig({
  css: ["@/assets/css/main.css"],
  vite: {
    plugins: [svgLoader()],
  },
})
